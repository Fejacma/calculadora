package br.com.aritmetica.service;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class MatematicaService {
    public RespostaDTO soma(EntradaDTO entradaDTO) {
        int numero = 0;
        for (int n : entradaDTO.getNumeros()) {
            numero += n;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO subtracao(EntradaDTO entradaDTO) {
        int numero = 0;
        boolean primeiroNumero = true;

        for (int n : entradaDTO.getNumeros()) {
            if (primeiroNumero) {
                numero = n;
                primeiroNumero = false;
            } else {
                numero -= n;
            }
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO multiplicacao(EntradaDTO entradaDTO) {
        int numero = 1;
        for (int n : entradaDTO.getNumeros()) {
            numero *= n;
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO divisao(EntradaDTO entradaDTO) {
        int numero = 1;
        boolean primeiroNumero = true;

        for (int n : entradaDTO.getNumeros()) {
            if (primeiroNumero){
                numero = n;
                primeiroNumero = false;
            }
            else {
                if (n > numero){
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Não pode - Dividor deve ser menos que o Dividendo");
                }
                else {
                    numero /= n;
                }
            }

        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }
}


