package br.com.aritmetica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication

public class AritmeticaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AritmeticaApplication.class, args);
	}

}
